api_key = "3ae12625fc0f43737af53c655accef55"

import requests
import json
import pprint


##### Useful URLS

# Base URL for accessing the TMDB API
movies_base = "https://api.themoviedb.org/3/"

# URL for getting information about Spider-Man: Across the Spider-Verse (movie id 569094)
movie_verse = movies_base + "movie/movie_id"

movie = input("Enter a movie: ")

# Additional URLS for searching
people_search = movies_base + "search/person"
movie_search = movies_base + "search/movie"

# URL for getting the cast and crew lists for movie 569094 (Spider-Man: Across the Spider-Verse)
#movie_credits = movies_base + "movie/movie_id/credits"

##### Code for accessing TMDB
# Request information about the movie, and pass the api_key as a parameter
parameter = {"api_key": api_key, "query": movie}
result_json = requests.get(movie_search, parameter)

# Convert the results from JSON to a dictionary
results = json.loads(result_json.text)
movie_id = results["results"][0]["id"]

movie_credits = movies_base + f"movie/{movie_id}/credits"

result_json = requests.get(movie_credits, parameter)

results = json.loads(result_json.text)



# Pretty print the dictionary so we can see what it looks like
pprint.pprint(results)
